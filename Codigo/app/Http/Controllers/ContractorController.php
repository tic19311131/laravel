<?php

namespace App\Http\Controllers;

use App\Models\Contractor;
use Illuminate\Http\Request;

class ContractorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //consultamos TODOS los contractors registrados
        $contractors = Contractor::all();
        return view('contractors.index', compact('contractors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //la accion que devuelve la VISTA pàra crear un nuevo contratista
        return view('contractors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //recibe los datos del  form de NUEVO contractor
        //validar--
        //crear el contractor
        $contractor = Contractor::create($request->all());
        return redirect()->route('contractor.index')->with('success', 'Contratista creado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function show(Contractor $contractor)
    {
        //muestra los detalle de un CONTRATISTA en particular
        $contractor = Contractor::findOrFail($contractor);
        return view('contractors.show', compact('contractor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function edit(Contractor $contractor)
    {
        //constulrams contratis
        //muestra los detalle de un CONTRATISTA en particular
        $contractor = Contractor::findOrFail($contractor);
        //la accion que devuelve la VISTA pàra crear un nuevo contratista
        return view('contractors.edit', compact('contractor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contractor $contractor)
    {
        //validar--
        //crear el contractor
        $contractor = Contractor::find($contractor);
        $contractor->name     = $request->name;
        $contractor->lastname = $request->lastname;
        $contractor->email    = $request->email;
        //..
        $contractor->save();
        return redirect()->route('contractor.index')->with('success', 'Contratista modificado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contractor  $contractor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contractor $contractor)
    {
        $contractor = Contractor::findOrFail($contractor);

        //si llega todo el BJETO.. sacr el ->id
        $contractor = Contractor::findOrFail($contractor->id);

        $contractor->delete();
        return redirect()->route('constractors.index')->with('success', 'Contratisa eliminado correctamente');
    }
}
