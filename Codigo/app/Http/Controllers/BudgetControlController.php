<?php

namespace App\Http\Controllers;

use App\Models\BudgetControl;
use Illuminate\Http\Request;

class BudgetControlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BudgetControl  $budgetControl
     * @return \Illuminate\Http\Response
     */
    public function show(BudgetControl $budgetControl)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BudgetControl  $budgetControl
     * @return \Illuminate\Http\Response
     */
    public function edit(BudgetControl $budgetControl)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BudgetControl  $budgetControl
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BudgetControl $budgetControl)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BudgetControl  $budgetControl
     * @return \Illuminate\Http\Response
     */
    public function destroy(BudgetControl $budgetControl)
    {
        //
    }
}
