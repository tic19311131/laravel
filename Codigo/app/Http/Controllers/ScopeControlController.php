<?php

namespace App\Http\Controllers;

use App\Models\ScopeControl;
use Illuminate\Http\Request;

class ScopeControlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ScopeControl  $scopeControl
     * @return \Illuminate\Http\Response
     */
    public function show(ScopeControl $scopeControl)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ScopeControl  $scopeControl
     * @return \Illuminate\Http\Response
     */
    public function edit(ScopeControl $scopeControl)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ScopeControl  $scopeControl
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ScopeControl $scopeControl)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ScopeControl  $scopeControl
     * @return \Illuminate\Http\Response
     */
    public function destroy(ScopeControl $scopeControl)
    {
        //
    }
}
