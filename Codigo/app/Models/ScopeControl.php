<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScopeControl extends Model
{
    use HasFactory;
    protected $fillable = [

        'progress',
        'date',
        'description',
        'user_id',


    ];

    public function user()
    {
        $this->belongsTo(User::class);
    }
}
