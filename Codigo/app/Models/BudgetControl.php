<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BudgetControl extends Model
{
    use HasFactory;
    protected $fillable = [

        'movement',
        'date',
        'amount',
        'description',

    ];
    public function user()
    {
        $this->belongsTo(User::class);
    }
}
