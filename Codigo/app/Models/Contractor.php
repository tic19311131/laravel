<?php

namespace App\Models;

use App\Models\Obra;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Contractor extends Model
{
    use HasFactory;
    protected $fillable = [

        'name',
        'lastname',
        'type',
        'rfc',
        'email',
        'street',
        'house_number',
        'residential',
        'postal_code',
        'city',
        'state',

    ];

    public function obras()
    {
        $this->hasMany(Obra::class);
    }
}
