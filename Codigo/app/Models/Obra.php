<?php

namespace App\Models;

use App\Models\Contractor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Obra extends Model
{
    use HasFactory;
    protected $fillable = [

        'name',
        'description',
        'type',
        'construcion_site',
        'start',
        'end',
        'contractor_id',
        'budget',

    ];

    public function contractor()
    {
        return $this->belongsTo(Contractor::class);
    }
}