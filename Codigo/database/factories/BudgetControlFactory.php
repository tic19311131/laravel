<?php

namespace Database\Factories;

use App\Models\BudgetControl;
use Illuminate\Database\Eloquent\Factories\Factory;

class BudgetControlFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BudgetControl::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
