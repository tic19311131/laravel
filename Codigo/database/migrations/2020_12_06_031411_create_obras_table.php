<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('obras', function (Blueprint $table)
        {
            $table->id();
            $table->string('name');
            $table->longText('description');
            $table->enum('type', ['CIVIL','ELECTRICA']);
            $table->longText('construction_site');
            $table->date('start');
            $table->date('end')->nullable();
            //todos los campos que seran FK deben ser unsignedBigInteger
            $table->unsignedBigInteger('contractor_id');
            $table->double('budget')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('obras');
    }
}