<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractors', function (Blueprint $table)
        {
            $table->id();
            $table->string('name');
            $table->string('lastname')->nullable();
            $table->enum('type', ['PERSONA_MORAL' . 'PERSONA_FISICA']);
            $table->string('rfc');
            $table->string('email')->unique();
            $table->string('cel');
            $table->string('street');
            $table->string('house_number');
            $table->string('residential')->nullable();
            $table->string('postal_code');
            $table->string('city');
            $table->string('state');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contractors');
    }
}