<?php

use App\Http\Controllers\ContractorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function ()
{
    return view('welcome');
});

//rutas para contratistas
Route::resource('contractors', ContractorController::class);

Route::get('/profile', function ()
{
    return view('profile');
});
Route::get('/ver/{id}/{email}', function ($id, $email)
{
    return "Estas viendo el perfil numero " . $id . " con el correo= " . $email;
});

Route::group(['prefix' => 'admin', 'as' => 'admin'], function ()
{
    Route::get('/', function ()
    {
        return view('layouts.main');
    });
    Route::get('/usuarios', function ()
    {
        return "Estas en USUARIOS";
    });
});

Route::get('main2', function ()
{
    return view('layouts.main2');
});